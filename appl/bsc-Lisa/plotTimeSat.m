%plots the average saturation in the whole domain of a 2D simulation
%different saturations possible, check implementation (and adjust
%legend accordingly)
%wetting saturation in column or plume
clear;

%for different time steps
load ../../build-cmake/appl/bsc-Lisa/timeAverageSat.out;
time=timeAverageSat(:,1);
sat= timeAverageSat(:,2);

hold on 
plot(time, sat)
hold off

xlabel('time [s]')
ylabel('depth-averaged wetting saturation [-]')
%axis([-inf inf 0 3.5])
%set(legend,'FontSize',25);
%set(findall(gca, 'Type', 'Line'),'LineWidth',2);
%set(gca,'fontsize',26)
