lambda=0.511/0.061;
rmax=1500;%m (variabel..)
H=15; %m
K=20; %mD
Qwell=120; %m^3/day
t=10000;  %day
phi=0.15;


tao=(Qwell*t)/(2*pi*H*phi*K);

for r=1:1:rmax  %eta
    n(r)=r/sqrt(K);
end

for r=1:1:rmax
    X(r)=((n(r)^2)/tao);
end

for r=1:1:rmax   %Xi^0.5
    X2(r)=((n(r)^2)/tao)^0.5;
end

for r=1:1:rmax
if  (1/(lambda-1))*((sqrt((2*lambda)/((n(r)^2)/tao)))-1)>=1
    h(r)=0;
else
    h(r)=1-(1/(lambda-1))*((sqrt((2*lambda)/((n(r)^2)/tao)))-1);
end 
end


plot(X2, h);


title('Beispiel Paper')
xlabel('dimensionsless distance')
ylabel('dimensionsless aquifer height')
axis([0 5 0 1])