%plots the average saturation in the whole domain of a 2D simulation
%different saturations possible, check implementation (and adjust
%legend accordingly)
%wetting saturation in column or plume
%different E
clear;

%for E1=-5.836e-4
load /temp/lisam/Results/E1/timeAverageSat1.out;

time1=timeAverageSat1(:,1);
sat1= timeAverageSat1(:,2);

H=30;
phi=0.2;
mu=5.23e-4;
K1=2e-13;
rho_w=991;
rho_n=59.2;
g=9.81;
lambda=2;

nmax=length(time1);
for n=1:1:nmax
    sat12(n)=((H*phi*mu)/(time1(n)*K1*(rho_w-rho_n)*g))^((lambda)/(2+3*lambda));
 
end

%for E2=-2.918e-4
load /temp/lisam/Results/E2/timeAverageSat.out;

time2=timeAverageSat(:,1);
sat2= timeAverageSat(:,2);

%for E3=-11.672e-4
load /temp/lisam/Results/E3/timeAverageSat.out;

time3=timeAverageSat(:,1);
sat3= timeAverageSat(:,2);

%for E3=-23.344e-4
load /temp/lisam/Results/E4/timeAverageSat.out;

time4=timeAverageSat(:,1);
sat4= timeAverageSat(:,2);

%for E3=-1.459e-4
load /temp/lisam/Results/E5/timeAverageSat.out;

time5=timeAverageSat(:,1);
sat5= timeAverageSat(:,2);

%PLOT
hold on
plot(time1, sat1, 'b')
plot(time1, sat12, 'b')
plot(time2, sat2, 'g')
plot(time3, sat3, 'm')
plot(time4, sat4, 'c')
plot(time5, sat5, 'r')
plot([1.72e6,1.72e6],[0,20],'k')
plot([0,4e6],[1,1],'k')
hold off



xlabel('tsim [s]')
ylabel('depth-averaged wetting saturation[-]')
%legend({'Swr*1; Sw1','Swr*2; Sw2', 'Swe*3; Sw3'},'Location','EastOutside')


axis([-inf inf 0 2])
%set(legend,'FontSize',25);
%set(findall(gca, 'Type', 'Line'),'LineWidth',2);
%set(gca,'fontsize',26)