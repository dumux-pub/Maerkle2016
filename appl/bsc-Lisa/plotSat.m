%plots the average saturation per column of a 2D simulation
%different saturations possible, check implementation (and adjust
%legend accordingly)
%wetting saturation in column or plume
clear;

%for different time steps
load ../../build-cmake/appl/bsc-Lisa/averageSatPlume.out;
%col1=averageSatPlume(1000,:);
%col2=averageSatPlume(2000,:);
%col3=averageSatPlume(5000,:);
%col4=averageSatPlume(9000,:);
%col5=averageSatPlume(10000,:);
col7=averageSatPlume(end,:);

hold on 
%plot(col1)
%plot(col2)
%plot(col3)
%plot(col4)
%plot(col5)
plot(col7)
hold off

xlabel('distance [m]')
ylabel('depth-averaged wetting saturation [-]')
%set(legend,'FontSize',25);
%set(findall(gca, 'Type', 'Line'),'LineWidth',2);
%set(gca,'fontsize',26)