/*****************************************************************************
 *   Copyright (C) 2007-2008 by Klaus Mosthaf                                *
 *   Copyright (C) 2007-2008 by Bernd Flemisch                               *
 *   Copyright (C) 2008-2009 by Andreas Lauser                               *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_TEST_ENERGY_STORAGE_PROBLEM_HH
#define DUMUX_TEST_ENERGY_STORAGE_PROBLEM_HH

#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif

#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/ch4.hh>

#include <dumux/porousmediumflow/2p/sequential/diffusion/cellcentered/pressureproperties.hh>
#include <dumux/porousmediumflow/2p/sequential/transport/cellcentered/properties.hh>
#include <dumux/porousmediumflow/2p/sequential/impes/problem.hh>
#include <dumux/porousmediumflow/2p/sequential/transport/cellcentered/evalcflfluxcoats.hh>
#include <dumux/porousmediumflow/2p/sequential/transport/cellcentered/evalcflfluxdefault.hh>

#include "test_2dspatialparams.hh"

namespace Dumux {

template<class TypeTag>
class TestEnergyStorageProblem;

//////////
// Specify the properties for the lens problem
//////////
namespace Properties {
NEW_TYPE_TAG(TestEnergyStorageProblem, INHERITS_FROM(FVPressureTwoP, FVTransportTwoP, IMPESTwoP, TestEnergyStorageSpatialParams));

// Set the grid type
SET_PROP(TestEnergyStorageProblem, Grid)
{
#if HAVE_UG
    typedef Dune::UGGrid<2> type;
#else
    typedef Dune::YaspGrid<2> type;
#endif
};

// Set the problem property
SET_TYPE_PROP(TestEnergyStorageProblem, Problem, Dumux::TestEnergyStorageProblem<TypeTag>);

// Set the wetting phase
SET_PROP(TestEnergyStorageProblem, WettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef FluidSystems::LiquidPhase<Scalar, Dumux::H2O<Scalar> > type;
};

// Set the non-wetting phase
SET_PROP(TestEnergyStorageProblem, NonwettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef FluidSystems::GasPhase<Scalar, Dumux::CH4<Scalar> > type;
};

// Enable gravity
SET_BOOL_PROP(TestEnergyStorageProblem, ProblemEnableGravity, true);
SET_TYPE_PROP(TestEnergyStorageProblem, EvalCflFluxFunction, Dumux::EvalCflFluxDefault<TypeTag>);
SET_SCALAR_PROP(TestEnergyStorageProblem, ImpetCFLFactor, 0.8);
SET_BOOL_PROP(TestEnergyStorageProblem, EnableCompressibility, false);
}
/*!
 * \ingroup DecoupledProblems
 */
template<class TypeTag>
class TestEnergyStorageProblem: public IMPESProblem2P<TypeTag> {
    typedef IMPESProblem2P<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView)GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, WettingPhase) WettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, NonwettingPhase) NonWettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };

    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,
        pwIdx = Indices::pwIdx,
        swIdx = Indices::swIdx,
        pressEqIdx = Indices::pressureEqIdx,
        satEqIdx = Indices::satEqIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::Intersection Intersection;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP(TypeTag, SolutionTypes)::PrimaryVariables PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;

    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename SpatialParams::MaterialLaw MaterialLaw;

    typedef std::array<int, dim> CellArray;

public:
    TestEnergyStorageProblem(TimeManager& timeManager, const GridView &gridView) :
    ParentType(timeManager, gridView)
    {
        int outputInterval = 0;
        if (ParameterTree::tree().hasKey("Problem.OutputInterval"))
        {
            outputInterval = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OutputInterval);
        }
        this->setOutputInterval(outputInterval);

        Scalar outputTimeInterval = 1e6;
        if (ParameterTree::tree().hasKey("Problem.OutputTimeInterval"))
        {
            outputTimeInterval = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OutputTimeInterval);
        }
        this->setOutputTimeInterval(outputTimeInterval);

        // store pointer to all elements in a multimap, elements belonging to the same column
        // have the same key, starting with key = 0 for first column
        // TODO: only works for equidistant grids
        //
        // iterate over all elements
        int j = 0;
        for (const auto& element : Dune::elements(this->gridView()))
        {
            // identify column number
            GlobalPosition globalPos = element.geometry().center();
            CellArray numberOfCellsX = GET_RUNTIME_PARAM_FROM_GROUP_CSTRING(TypeTag, CellArray, "Grid", Cells);
            double deltaX = this->bBoxMax()[0]/numberOfCellsX[0];

            j = round((globalPos[0] - (deltaX/2.0))/deltaX);

            mapColumns_.insert(std::make_pair(j, element));
            dummy_ = element;
        }
        averageSatPlume_.resize(j+1);

        //calculate length of capillary transition zone (CTZ)
        Scalar swr = this->spatialParams().materialLawParams(dummy_).swr();
        Scalar snr = this->spatialParams().materialLawParams(dummy_).snr();
        Scalar satW1 = 1.0 - snr;
        Scalar satW2 = swr + 0.1*(1.0-swr-snr);
        Scalar pc1 = MaterialLaw::pc(this->spatialParams().materialLawParams(dummy_), satW1);
        Scalar pc2 = MaterialLaw::pc(this->spatialParams().materialLawParams(dummy_), satW2);
        GlobalPosition globalPos = dummy_.geometry().center();
        Scalar pRef = referencePressureAtPos(globalPos);
        Scalar tempRef = temperatureAtPos(globalPos);
        Scalar densityW = WettingPhase::density(tempRef, pRef);
        Scalar densityNw = NonWettingPhase::density(tempRef, pRef);
        CTZ_ = (pc2-pc1)/((densityW-densityNw)*this->gravity().two_norm());
        std::cout << "CTZ " << CTZ_ << std::endl;

        outputFile_.open("averageSatPlume.out", std::ios::trunc);
        outputFile_.close();
        outputFile_.open("timeAverageSat.out", std::ios::trunc);
        outputFile_.close();

        //for adaptivity:
        GridCreator::grid().globalRefine(GET_PARAM_FROM_GROUP(TypeTag, int, GridAdapt, MaxLevel));
        this->setGrid(GridCreator::grid());
    }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    void init()
    {
        ParentType::init();

        bool plotFluidMatrixInteractions = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Output, PlotFluidMatrixInteractions);

        // plot the Pc-Sw curves, if requested
        if(plotFluidMatrixInteractions)
            this->spatialParams().plotMaterialLaw();
    }

    /*!
     * \name Problem parameters
     */
// \{
    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        if (ParameterTree::tree().hasKey("Problem.Name"))
        {
            std::string fileName(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name));
            return fileName.c_str();
        }
        else
        {
            return "test_impes2p";
        }
    }
    bool shouldWriteRestartFile() const
    {
        return false;
    }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return 326.0; // -> 41,85°C
    }

// \}

    Scalar referencePressureAtPos(const GlobalPosition& globalPos) const
    {
        return 1.0e7; //
    }

    void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition& globalPos) const
    {
        if (globalPos[0] > this->bBoxMax()[0] - eps_)
        {
            bcTypes.setAllDirichlet();
        }
        // all other boundaries
        else
        {
            bcTypes.setAllNeumann();
        }
    }

    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition& globalPos) const
    {
        values = 0;

        if (globalPos[0] > this->bBoxMax()[0] - eps_)
        {
            if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
            {
                Scalar pRef = referencePressureAtPos(globalPos);
                Scalar temp = temperatureAtPos(globalPos);

                values[pwIdx] = (pRef + (this->bBoxMax()[dim-1] - globalPos[dim-1])
                                       * WettingPhase::density(temp, pRef)
                                       * this->gravity().two_norm());
            }
            else
            {
                values[pwIdx] = 1.0e7;
            }
            values[swIdx] = 1.0;
        }
        else
        {
            values[pwIdx] = 1.0e7;
            values[swIdx] = 0.0;
        }
    }

//! set neumann condition for phases (flux, [kg/(m^2 s)])
    void neumannAtPos(PrimaryVariables &values, const GlobalPosition& globalPos) const
    {
        values = 0.0;
        if (globalPos[0] < eps_)
        {
            values[nPhaseIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, BoundaryConditions, Injectionrate);
        }
    }

    void source(PrimaryVariables &values, const Element& element) const
    {
        values = 0.0;
    }

    void initialAtPos(PrimaryVariables &values,
            const GlobalPosition& globalPos) const
    {
        values[pressEqIdx] = 1.0e7;
        values[swIdx] = 1.0;
    }

    void postTimeStep()
    {
        ParentType::postTimeStep();

        //initialize/reset average column saturation
        for (int i = 0; i < averageSatPlume_.size(); i++)
        {
            averageSatPlume_[i] = 0.0;
        }

        Scalar averageSatTotal = 0.0;
        Scalar gasPlumeVolume = 0.0;// volume of gas plume
        for (int i = 0; i != averageSatPlume_.size(); ++i)
        {
            Scalar totalVolume = 0.0;// total volume of column
            Scalar gasVolume = 0.0;// volume with SatN>0.0 in one column;
            typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(i);
            for (; it != mapColumns_.upper_bound(i); ++it)
            {
                int globalIdxI = this->variables().index(it->second);
                Scalar satW = this->variables().cellData(globalIdxI).saturation(wPhaseIdx);
                Scalar satNw = this->variables().cellData(globalIdxI).saturation(nPhaseIdx);
                Scalar volume = it->second.geometry().volume();
                totalVolume += volume;
                if(satNw>1e-3)
                {
                    averageSatPlume_[i] += satW * volume;
                    gasVolume += volume;
                    averageSatTotal += satW * volume;
                    gasPlumeVolume += volume;
                }
            }
            averageSatPlume_[i] = averageSatPlume_[i]/gasVolume;
            outputFile_.open("averageSatPlume.out", std::ios::app);
            outputFile_ << " " << averageSatPlume_[i];
            outputFile_.close();
        }

        averageSatTotal = averageSatTotal/gasPlumeVolume;

        outputFile_.open("timeAverageSat.out", std::ios::app);
        outputFile_ << this->timeManager().time() << " " << averageSatTotal << std::endl;
        outputFile_.close();

        outputFile_.open("averageSatPlume.out", std::ios::app);
        outputFile_ << " " << std::endl;
        outputFile_.close();
    }


private:
    static constexpr Scalar eps_ = 1e-6;
    static constexpr Scalar eps2_ = 1e-6;
    static constexpr Scalar depthBOR_ = 1000;

    std::multimap<int, Element> mapColumns_;
    std::vector<double> averageSatPlume_;
    std::ofstream outputFile_;
    Element dummy_;
    Scalar CTZ_;
};
}
 //end namespace

#endif
