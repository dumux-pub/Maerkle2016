%plots the average saturation in the whole domain of a 2D simulation
%different saturations possible, check implementation (and adjust
%legend accordingly)
%wetting saturation in column or plume
%different K
clear;

%for different time steps for K1
load /temp/lisam/Results/K1/timeAverageSat.out;

time1=timeAverageSat(:,1);
sat1= timeAverageSat(:,2);

H=30;
phi=0.2;
mu=5.23e-4;
K1=2e-13;
rho_w=991;
rho_n=59.2;
g=9.81;
lambda=2;

nmax=length(time1);
for n=1:1:nmax
    sat12(n)=((H*phi*mu)/(time1(n)*K1*(rho_w-rho_n)*g))^((lambda)/(2+3*lambda));
 
end

%tsim/tseg
nmax=length(time1);
for n=1:1:nmax
    time12(n)=(time1(n)/(1.72e6));

end

%for different time steps for K2
load /temp/lisam/Results/K2/timeAverageSat22.out;

time2=timeAverageSat22(:,1);
sat2= timeAverageSat22(:,2);

H=30;
phi=0.2;
mu=5.23e-4;
K2=4e-13;
rho_w=991;
rho_n=59.2;
g=9.81;
lambda=2;

nmax=length(time2);
for n=1:1:nmax
    sat22(n)=((H*phi*mu)/(time2(n)*K2*(rho_w-rho_n)*g))^((lambda)/(2+3*lambda));
 
end

%tsim/tseg
nmax=length(time2);
for n=1:1:nmax
    time22(n)=(time2(n)/(8.6e5));

end

%for different time steps for K3
load /temp/lisam/Results/K3/timeAverageSat3.out;

time3=timeAverageSat3(:,1);
sat3= timeAverageSat3(:,2);

H=30;
phi=0.2;
mu=5.23e-4;
K3=8e-13;
rho_w=991;
rho_n=59.2;
g=9.81;
lambda=2;

nmax=length(time3);
for n=1:1:nmax
    sat32(n)=((H*phi*mu)/(time3(n)*K3*(rho_w-rho_n)*g))^((lambda)/(2+3*lambda));
 
end

%tsim/tseg
nmax=length(time3);
for n=1:1:nmax
    time32(n)=(time3(n)/(4.3e5));

end

%PLOT
hold on
plot(time12, sat1, 'b')
plot(time12, sat12, 'b')
plot(time22, sat2, 'g')
plot(time22, sat22, 'g')
plot(time32, sat3, 'm')
plot(time32, sat32, 'm')
plot([0,2],[1,1],'k')
plot([1,1],[0,20],'k')
hold off



xlabel('tsim/tseg [s]')
ylabel('depth-averaged wetting saturation[-]')
%legend({'Swr*1; Sw1','Swr*2; Sw2', 'Swe*3; Sw3'},'Location','EastOutside')


axis([-inf inf 0 2])
%set(legend,'FontSize',25);
%set(findall(gca, 'Type', 'Line'),'LineWidth',2);
%set(gca,'fontsize',26)