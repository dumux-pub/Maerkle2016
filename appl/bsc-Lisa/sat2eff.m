%plots the average saturation in the whole domain of a 2D simulation
%different saturations possible, check implementation (and adjust
%legend accordingly)
%wetting saturation in column or plume
clear;

%for different time steps
load ../../build-cmake/appl/bsc-Lisa/timeAverageSat.out;
time=timeAverageSat(:,1);
sat= timeAverageSat(:,2);

H=30;
phi=0.2;
mu=5.23e-4;
K=2e-13;
rho_w=991;
rho_n=59.2;
g=9.81;
lambda=2;

nmax=length(time);
for n=1:1:nmax
    sat2(n)=((H*phi*mu)/(time(n)*K*(rho_w-rho_n)*g))^((lambda)/(2+3*lambda));
 
end


hold on
plot(time, sat,'b')
plot(time, sat2,'m')
plot([1.72e6,1.72e6],[0,20],'k')
plot([0,5e6],[1,1],'k')
hold off

xlabel('time [s]')
ylabel('depth-averaged wetting saturation[-]')
legend({'Swr*','Sw'},'Location','EastOutside')


axis([-inf inf 0 2])
%set(legend,'FontSize',25);
%set(findall(gca, 'Type', 'Line'),'LineWidth',2);
%set(gca,'fontsize',26)